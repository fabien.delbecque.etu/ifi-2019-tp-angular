import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { MatExpansionModule } from '@angular/material/expansion';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDividerModule } from '@angular/material/divider';
import {MatInputModule} from '@angular/material/input';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { ArticlesListComponent } from './components/articles-list/articles-list.component';
// import { HeaderComponent } from './components/header/header.component';
// A compléter

@NgModule({
  declarations: [
    AppComponent,
    ArticlesListComponent,
//    HeaderComponent,
// A compléter
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatExpansionModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatDividerModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    RouterModule.forRoot([
      { path: '', component: ArticlesListComponent },
     // { path: 'articles/:articleId', component: ArticleDetailComponent }
    ])
  ],
  providers: [],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
