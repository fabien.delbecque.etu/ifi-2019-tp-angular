import { Injectable } from '@angular/core';
import { Article } from 'src/app/shared/models/article.model';
import { of, Observable, Subject, ReplaySubject } from 'rxjs';
import { articles } from 'src/app/shared/mocks/articles';

/** ArticlesService qui renvoie la liste des articles */
@Injectable({
  providedIn: 'root'
})
export class ArticlesService {
  private articles: Article[] = articles;

  // TODO A SUPPRIMER pour la partie filter
  constructor() {

  }

  getArticles(): Observable<Article[]> {
    return of(this.articles);
  }

  // TODO A DECOMMENTER pour la partie filter

  /*

  // Liste des articles affiché
  private displayedArticles: Article[] = articles;
  private articlesSubject: Subject<Article[]> = new ReplaySubject<Article[]>(1);

  // Changer le code du constructeur
  constructor() {
    // Initialiser la première valeur pour les premiers abonnements
    this.articlesSubject.next(this.displayedArticles);
  }

  getArticles(): Observable<Article[]> {
    return this.articlesSubject.asObservable();
  }

  filterArticles(val: string) {
    if (val === '') {
      // TODO Remettre la valeur initiale de this.displayedArticles si le filter est vide
    } else {
      // TODO Changer this.displayedArticles en filtrant les valeurs de this.articles en minuscule avec la valeur 'val' en minuscule
    }
    // Pour update la valeur et la transmettre aux abonnés
    this.articlesSubject.next(this.displayedArticles);
  }


  /* Exemple si jamais nous avions un back, ce n'est pas notre cas ici
      Ceci est un code d'internet, dans le cours nous avons vu les HTTPClient.
      Il vaut mieux utiliser le HTTPClient.

    import { Http, Response } from '@angular/http';

    constructor(private http: Http) { }

    getArticles() {
      return this.http.get('/api/articles')
        .map((res: Response) => res.json().response);
    }
    */
}
